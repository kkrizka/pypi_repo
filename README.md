# python_package_repository

Gitlab Selfhosted private python pypi like repository for ColdJigDCS code.
This allowsd one to install via pip as with any other python package
instead of having to clone the entire repository.

At the moment, only the hardware drivers for the Warwock ColdJig are stored here.

